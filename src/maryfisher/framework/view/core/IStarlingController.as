package maryfisher.framework.view.core {
	import org.osflash.signals.Signal;
	
	/**
	 * ...
	 * @author mary_fisher
	 */
	public interface IStarlingController {
		function get onFinished():Signal;
	}
	
}